# Examen de Curso Desarrollo y Programación de Visualización de Datos Geográficos en Internet.
Cantidad de vocales en una noticia del sitio web de gob.cl (https://www.gob.cl/noticias/sernac-buscara-compensaciones-para-los-consumidores-afectados-por-corte-de-agua-en-osorno/)


**Misión**
1. Fuente de datos
2. Procesar algún dato de esa fuente
3. Exportar datos a un csv u otro formato
4. Mostrarlo en un gráfico o mapa
5. Gitlab (o github)
6. Netlify (o gitbub pages)

**Resumen**
Crear una one page mostrando los datos recolectados (vocales del sitio gob.cl) a través de un gráfico utilizando chart.js.

FUENTE: https://www.gob.cl/noticias/sernac-buscara-compensaciones-para-los-consumidores-afectados-por-corte-de-agua-en-osorno/

**Pasos a seguir**
1. Creación de repositorio (gitlab) con archivo readme (https://gitlab.com/Luarte/examen-datos)
2. Clonar repositorio en escritorio
3. Creación de index.html, index.js, carpeta css, carpeta js, carpeta images
4. Creación de package.json
5. Publicar en netlify (para implementarlo temporalmente)


**Recursos necesarios**
1. node
2. npm
3. cheerio
4. request
5. chart
6. cvs-writer


**Selectores**
1. Vocales

2. npm

**Visualización**
https://fervent-northcutt-bf548e.netlify.com
